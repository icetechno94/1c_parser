from openpyxl import load_workbook
import csv


# Путь до файла выгрузки из 1с
file_path = './common/main_xls.xlsx'
# Путь до конечного файла
result_file_path = './result/result_data.csv'
# Название страницы
sheet_name = 'Sheet1'
# Минимальная строка с
min_row = 10
# Массив для заполнения
result_data = [{"name": "sku", "price": "regular_price"}]
# index колонки с ценой
price_index = 4
# index колонки с названием товара
name_index = 1

# Загрузка данных из файла
wb = load_workbook(file_path)

sheet = wb[sheet_name]
max_row = sheet.max_row
for row_index in range(min_row, max_row):
    name = sheet.cell(row=row_index, column=name_index).value.split(' ')[-1]
    price = sheet.cell(row=row_index, column=price_index).value
    if name and price:
        result_data.append({'name': name, 'price': price})
outfile = open(result_file_path, 'w')
fieldnames = ['name', 'price']
writer = csv.DictWriter(outfile, fieldnames=fieldnames, quotechar='"', quoting=csv.QUOTE_ALL)
for item in result_data:
    writer.writerow(item)
outfile.close()
