# 1c_parser

# Installation

Install virtualenv for python3

Initialize virtualenv in root folder with name ``venv``

Enable virtualenv ``source venv/bin/activate``

Then run ``pip freeze > requirements.txt``

# Usage

Add 1c import to common folder with name ``main_xls.xlsx``

Then run ``python test.py`` 